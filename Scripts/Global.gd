extends Node


var REGEX: RegEx = RegEx.new()
const _regex_string: String = "(\\d)(?=(\\d\\d\\d)+([^\\d]|$))"
var _is_regex_ok: bool = false


const NUMBERS: Array = [
	{
		"cost": 30_000,
		"texture": preload("res://Resourses/Sprites/numbers/1-min.png"),
	},
	{
		"cost": 300_000,
		"texture": preload("res://Resourses/Sprites/numbers/2-min.png"),
	},
	{
		"cost": 15_000,
		"texture": preload("res://Resourses/Sprites/numbers/3-min.png"),
	},
	{
		"cost": 15_000,
		"texture": preload("res://Resourses/Sprites/numbers/4-min.png"),
	},
	{
		"cost": 750_000,
		"texture": preload("res://Resourses/Sprites/numbers/5-min.png"),
	},
	{
		"cost": 5_000_000,
		"texture": preload("res://Resourses/Sprites/numbers/6-min.png"),
	},
	{
		"cost": 3_000_000,
		"texture": preload("res://Resourses/Sprites/numbers/7-min.png"),
	},
]


# BUILTINS -------------------------


func _init() -> void:
	_is_regex_ok = true if REGEX.compile(_regex_string) == OK else false


# METHODS -------------------------


# форматирование числа, проставление знаков между порядками
func format_number(number: int, delimiter: String = ",") -> String:
	var number_string: String = str(number)
	if _is_regex_ok:
		return REGEX.sub(number_string, "$1" + delimiter, true)
	else:
		return number_string


# SETGET -------------------------


# SIGNALS -------------------------


