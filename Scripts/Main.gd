extends Control


var SUM_FOR_ROUND: int = 0


# BUILTINS -------------------------


func _ready() -> void:
	($AnimationPlayer as AnimationPlayer).play("default")

	var _c: int = ($Board as Control).connect("removable_image_was_erased", self, "_on_Board_removable_image_was_erased")
	_c = ($Board as Control).connect("board_ckecked", self, "_on_Board_board_ckecked")
	_c = ($Footer as Control).connect("button_check_pressed", self, "_on_Footer_button_check_pressed")
	_c = ($Win as Control).connect("win_button_collect_pressed", self, "_on_Win_win_button_collect_pressed")
	_c = ($Lines as Control).connect("lines_drawing_ended", self, "_on_Lines_lines_drawing_ended")


# METHODS -------------------------


func _get_score_sum(combitation: Array) -> void:
	SUM_FOR_ROUND = 0
	for i in combitation:
		SUM_FOR_ROUND += i.cost

# SETGET -------------------------


# SIGNALS -------------------------


func _on_Board_removable_image_was_erased() -> void:
	($Footer as Control).set("is_button_check_disabled", false)


func _on_Board_board_ckecked(combinations: Array) -> void:
	($Lines as Control).call_deferred("draw_lines", combinations)
	_get_score_sum(combinations)


func _on_Footer_button_check_pressed() -> void:
	($Board as Control).call_deferred("check_board")


func _on_Win_win_button_collect_pressed() -> void:
	($Footer as Control).set("is_button_check_disabled", true)
	($Board as Control).call("prepare_board")
	($Lines as Control).call_deferred("clear_lines")
	($Header as Control).call_deferred("encrease_score", SUM_FOR_ROUND)


func _on_Lines_lines_drawing_ended() -> void:
	($Win as Control).call_deferred("show_win", SUM_FOR_ROUND)
