extends Control


signal win_button_collect_pressed


const TWEEN_SPEED: float = 0.3


# BUILTINS -------------------------


func _ready() -> void:
	self.hide()
	self.modulate.a = 0.0


# METHODS -------------------------


func show_win(num: int) -> void:
	($BtnCollect/Label as Label).text = Global.call("format_number", num)
	self.show()
	var _i: bool = ($Tween as Tween).interpolate_property(self, "modulate:a", null, 1.0, TWEEN_SPEED)
	if not ($Tween as Tween).is_active():
		_i = ($Tween as Tween).start()


func hide_win() -> void:
	var _i: bool = ($Tween as Tween).interpolate_property(self, "modulate:a", null, 0.0, TWEEN_SPEED)
	if not ($Tween as Tween).is_active():
		_i = ($Tween as Tween).start()
	yield($Tween as Tween, "tween_all_completed")
	self.hide()


# SETGET -------------------------


# SIGNALS -------------------------


func _on_BtnCollect_pressed() -> void:
	emit_signal("win_button_collect_pressed")
	hide_win()
