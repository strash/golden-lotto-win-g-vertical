extends Control


signal button_check_pressed


var is_button_check_disabled: bool = true setget set_button_check_disabled, get_button_check_disabled


# BUILTINS -------------------------


func _ready() -> void:
	set_button_check_disabled(is_button_check_disabled)


# METHODS -------------------------


# SETGET -------------------------


func set_button_check_disabled(state: bool) -> void:
	is_button_check_disabled = state
	($Button as Button).disabled = state
	($Particles2D as Particles2D).emitting = not state


func get_button_check_disabled() -> bool:
	return is_button_check_disabled


# SIGNALS -------------------------


func _on_Button_pressed() -> void:
	emit_signal("button_check_pressed")
