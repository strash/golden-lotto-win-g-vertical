extends Control


signal removable_image_was_erased
signal board_ckecked


const Number: PackedScene = preload("res://Scenes/Number.tscn")
const ERASER_MASK: Texture = preload("res://Resourses/Sprites/eraser-mask-lookup.png")
onready var REMOVABLE_IMAGE_SRC: Texture = ($Bg/RemovableLayer as TextureRect).texture
# массив векторов, в которых x, y - позиция пиксела
# в нем хранятся только те пиксели, в которых альфа равна 0
# массив уменьшен для оптимизации и хранит только четные пиксели
# проще говоря - маска
var eraser_positions_alfa: PoolVector2Array = PoolVector2Array([])
var removable_image: Image = Image.new()
var removable_image_texture: ImageTexture = ImageTexture.new()

const MIN_ERASED_PERCENTAGE: float = 0.8
var REMOVABLE_IMAGE_PIXEL_COUNT: int

const MIN_NUMBER_OF_MATCHES: int = 3

var is_board_locked: bool = false


# BUILTINS -------------------------


func _ready() -> void:
	randomize()
	prepare_board()
	REMOVABLE_IMAGE_PIXEL_COUNT = removable_image.get_width() * removable_image.get_height()
	_eraser_lookup()


func _gui_input(event) -> void:
	if is_board_locked:
		return
	if event is InputEventScreenTouch and event.is_pressed() or event is InputEventScreenDrag:
		var origin_position: Vector2 = event.position - ($Bg/RemovableLayer as TextureRect).get_global_rect().position
		_erase_image(origin_position)
		_update_removable_image()
	elif event is InputEventScreenTouch and not event.is_pressed():
		_removable_image_lookup()


# METHODS -------------------------


func prepare_board() -> void:
	($Bg/RemovableLayer as TextureRect).texture = REMOVABLE_IMAGE_SRC
	removable_image = REMOVABLE_IMAGE_SRC.get_data()
	removable_image.convert(Image.FORMAT_RGBA8)
	is_board_locked = false
	if ($Bg/NumbersGrid as GridContainer).get_child_count() > 0:
		for i in ($Bg/NumbersGrid as GridContainer).get_children():
			i.call("remove_number")
	_generate_numbers_to_board()


func check_board() -> void:
	var combinations: Array = []
	var num_size: Vector2 = (($Bg/NumbersGrid as GridContainer).get_child(0) as TextureRect).get_rect().size
	for i in ($Bg/NumbersGrid as GridContainer).get_children():
		var num_pos_center: Vector2 = i.get_global_rect().position + num_size / 2.0
		var count: int = 0
		if not combinations.empty():
			for j in combinations:
				if i.get("id") == j.id:
					j.pos_center.append(num_pos_center)
					count += 1
		if combinations.empty() or count == 0:
			var combination: Dictionary = {}
			combination.id = i.get("id")
			combination.cost = i.get("cost")
			combination.pos_center = []
			combination.pos_center.append(num_pos_center)
			combinations.append(combination)
	emit_signal("board_ckecked", _clean_combinations(combinations))


func _clean_combinations(combinations: Array) -> Array:
	var cleaned_combinations: Array = []
	for i in combinations:
		if i.pos_center.size() >= MIN_NUMBER_OF_MATCHES:
			cleaned_combinations.append(i)
	return cleaned_combinations


func _generate_numbers_to_board() -> void:
	for i in 16:
		var random_number: int = randi() % Global.get("NUMBERS").size()
		var number: TextureRect = Number.instance()
		var random_params: Dictionary = Global.get("NUMBERS")[random_number]
		number.texture = random_params.texture
		number.set("id", random_number)
		number.set("cost", random_params.cost)
		($Bg/NumbersGrid as GridContainer).add_child(number)


func _removable_image_lookup() -> void:
	var count: float = 0.0
	removable_image.lock()
	for x in removable_image.get_width():
		for y in removable_image.get_height():
			if removable_image.get_pixel(x, y).a == 0:
				count += 1.0
	removable_image.unlock()
	if count / REMOVABLE_IMAGE_PIXEL_COUNT >= MIN_ERASED_PERCENTAGE:
		emit_signal("removable_image_was_erased")
		is_board_locked = true


func _eraser_lookup() -> void:
	var eraser_mask_image: Image = ERASER_MASK.get_data()
	eraser_mask_image.lock()
	var e_w: int = eraser_mask_image.get_width()
	var e_h: int = eraser_mask_image.get_height()
	var e_w_off: int = int(floor(e_w / 2.0))
	var e_h_off: int = int(floor(e_h / 2.0))
	for x in e_w:
		for y in e_h:
			var pixel: Color = eraser_mask_image.get_pixel(x, y)
			if pixel.a == 0.0 and fmod(x, 2) == 0 and fmod(y, 2) == 0:
				eraser_positions_alfa.append(Vector2(x - e_w_off, y - e_h_off))
	eraser_mask_image.unlock()


func _erase_image(pos: Vector2) -> void:
	removable_image.lock()
	for i in eraser_positions_alfa:
		var x: int = int(pos.x) + i.x
		var y: int = int(pos.y) + i.y
		if _is_pixel_inside_the_image(x, y):
			removable_image.set_pixel(x, y, Color(1.0, 1.0, 1.0, 0.0))
	removable_image.unlock()


func _is_pixel_inside_the_image(x: int, y: int) -> bool:
	if x >= 0 and y >= 0 and x < removable_image.get_width() and y < removable_image.get_height():
		return true
	return false


func _update_removable_image() -> void:
	removable_image_texture.create_from_image(removable_image)
	($Bg/RemovableLayer as TextureRect).texture = removable_image_texture


# SETGET -------------------------


# SIGNALS -------------------------


