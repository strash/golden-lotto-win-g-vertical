extends Control


var SCORE: int = 0 setget set_score
var LEVEL: int = 0 setget set_level
const LEVEL_STEP: int = 10_000_000

const TWEEN_SPEED: float = 1.5


# BUILTINS -------------------------


func _ready() -> void:
	set_score(SCORE)
	set_level(LEVEL)


# METHODS -------------------------


func encrease_score(new_score: int) -> void:
	var _i: bool = ($Tween as Tween).interpolate_method(self, "set_score", SCORE, SCORE + new_score, TWEEN_SPEED)
	if not ($Tween as Tween).is_active():
		_i = ($Tween as Tween).start()
	set_level(floor(float(SCORE + new_score) / LEVEL_STEP) as int)


func _set_progress() -> void:
	var score: float = fmod(SCORE, LEVEL_STEP) * 100.0 / LEVEL_STEP
	($MarginContainer/HBoxContainer/Progress/TextureProgress as TextureProgress).value = score


# SETGET -------------------------


func set_score(new_score: int) -> void:
	SCORE = new_score
	($MarginContainer/HBoxContainer/Score/Label as Label).text = Global.call("format_number",new_score)
	_set_progress()


func set_level(new_level: int) -> void:
	LEVEL = new_level
	($MarginContainer/HBoxContainer/Level/NinePatchRect/Label as Label).text = str(new_level)


# SIGNALS -------------------------


